# KiwiIRC - Google Analytics Plugin

### Status - In development

This plugin adds Google Analytics to KiwiIRC. 

This plugin requires yarn.

### Installation

    $ git clone https://code.libertas.tech/TurtleChatRooms/kiwiirc-plugin-google_analytics.git
    $ cd kiwiirc-plugin-google_analytics
    $ yarn && yarn build

Copy the built `dist/*.js` file to your kiwi plugins folders.

Next, add the following config parameter to /your/kiwi/folder/static/config.json

    "plugins": [
        {"name": "google_analytics", "url": "static/plugins/plugin-google_analytics.min.js"} 
    ],
    "google_analytics": {
      "trackingID": "UA-123456789-1"
    }
