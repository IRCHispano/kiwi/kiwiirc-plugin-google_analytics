kiwi.plugin('google_analytics', function(kiwi) {
  const settings = kiwi.state.setting('google_analytics')
  if (settings.trackingID) {
    let gtagjs = document.createElement('script')
    let gascript = document.createElement('script')
    gtagjs.setAttribute('async', '')
    gtagjs.setAttribute('src', `https://www.googletagmanager.com/gtag/js?id=${settings.trackingID}`)
    gascript.innerHTML = `
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());

      gtag('config', '${settings.trackingID}');
    `
    document.getElementsByTagName('head')[0].appendChild(gtagjs)
    document.getElementsByTagName('head')[0].appendChild(gascript)
  }
})